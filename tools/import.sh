#!/usr/bin/env ruby
# coding: utf-8
USAGE=<<ENDDOC
#
# Importing raw images in the internal Catalog.
#
# Usage:
#
#     import [source folder] [catalog folder]
#
#   This will try to import/convert all images from the source folder
#   into the internal formats needed by shop for displaying the
#   listing/homepage in the correct size for all the supported
#   platform (e.g. mobile, tablets, large desktps etc.).
#
# Example:
#
#   A simple example:
#
#     import ~/Temp/MyImages /opt/mongateau/catalog
#
#   The above will load, convert and move all the images in the ~/Temp/MyImages
#   in the Catalog folder.
#
# Structure of the Catalog:
#
#   The catalog folder is organized in a way to store the same image in
#   different sizes in the correct folders. This is an example of its layout
#   with a single image in it:
#
#     catalog/
#       ├── images
#       │   ├── desktop
#       │   │   └── IMG_2098_360_480.JPG
#       │   ├── mobile
#       │   │   └── IMG_2098_300_400.JPG
#       │   └── tablet
#       │       └── IMG_2098_320_420.JPG
#       ├── products.json
#       └── tags.json
#
#   The raw image (IMG_2098.JPG) has been converted in three sizes we need for
#   different medias and stored in the proper folders.
#
# Notes:
#   - The `import` operation is always *destructive*: it will wipe out
#     the old catalog/images and create a new one from the raw content
#     in the source folder. This means that the source folder has to
#     contain all the images needed by the shop.
#
ENDDOC

require 'fileutils'
require 'ptools'
require 'colorize'

def setup_images_repo(catalog_images_folder)
  if Dir.exists?(catalog_images_folder)
    puts "Folder [#{catalog_images_folder}] already exists. Wiping out ..."
    FileUtils.rm_rf(catalog_images_folder)
  end

  Dir.mkdir(catalog_images_folder)
  Dir.mkdir(catalog_images_folder + "/mobile")
  Dir.mkdir(catalog_images_folder + "/desktop")
  Dir.mkdir(catalog_images_folder + "/tablet")

  puts "Setup #{catalog_images_folder} complete"
end

# Check if we have ImageMagick 'convert' in the PATH
abort "ImageMagick `convert` is not available" if !File.which("convert")

# Check for input arguments
abort USAGE if ARGV.length != 2
source_folder = ARGV[0]
catalog_folder = ARGV[1]
puts "Source folder : #{source_folder}"
puts "Catalog folder: #{catalog_folder}"

abort "Source folder does not exists (#{source_folder})".red if ! Dir.exists?(source_folder)
abort "Catalog folder does not exists (#{catalog_folder})".ref if ! Dir.exists?(catalog_folder)

# Make sure to prepare a clean Catalog/images folder
catalog_images_folder = catalog_folder + "/images"
setup_images_repo(catalog_images_folder)

# Scan the source folder and for each image convert it and move to the proper
# folder in the Catalog.
convert = File.which("convert")
puts "Using ImageMagick `convert` from #{convert}"
Dir.foreach(source_folder).select { |f| f != ".." and f != "." }.each do |f|
  puts "Processing #{f} ..."

  src = File.new("#{source_folder}/#{f}")
  [["360x480", "80", "desktop"], ["300x400", "40", "mobile"], ["320x420", "60", "tablet"]].each do |size, jpegQuality, subfolder|
    out = "#{File.basename(src, ".*")}_#{size}.jpg"
    # cmd = "#{convert} #{source_folder}/#{f} -resize #{size} #{catalog_images_folder}/#{subfolder}/#{out}"
    cmd = "#{convert} #{source_folder}/#{f} -quality #{jpegQuality} -resize #{size} #{catalog_images_folder}/#{subfolder}/#{out}"
    puts " #{cmd}"
    abort "Error while converting #{f}. Aborting ..." if ! system(cmd)
  end
end

puts "Done".green
