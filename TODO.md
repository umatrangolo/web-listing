# TODO

v Play/Lucene/fs/json/Twirl templates
v Lucene indexing of a static file configuring each product
  Each product will be represented like:
  {
    "key": ..
    "description": ..
    "image": .. --> this have to be present in the /public folder
    "price": ..
    "tags": [ .. ]
    ..
  }
  We will index by tags and a listing page will be just the rendering of a Lucene query.
x Clicking on a product will show a simple detail page.
v No cart/payment: just email/phone num to call to order.
+ Indexed by Google (SEO ??)
x Leave your number/email to be contacted
v Pinterest support
+ Social/email extensions (same app or another one?)
  - New cake available sending an email with the picture of the new cake when it gets published
  - New cake post to all out Facebook followers
v Decouple products from assets: all products (json + images) have to be in a separate folder
  and web-listing has to watch that folder and reindex on any change.
  E.g.:
  /web-listing
    /products
      product1.json
      product1.jpg
      product2.json
      product2.jpg
      ...
  Every time an item gets added/removed/updated we reindex and hot swap the products. No more pushing
  images/json in the assets/conf and redeploy for each new item(s).
+ AdWords relevant landing page for each configured tag
v Move to Play 2.4.0 wout Activator
v Move to Twitter Bootstrap
x Rewrite listing with Tremula.js
v Adj listing columns to window width (needed for mobile)
+ More structured site
  - Home page with the best hand picked cakes --> This will be the blog!
  - Listing pages with all the cakes
+ Rename!
v Move from ScalDI to Play DI
+ Homepage!
+ Use Stripe for payment ???
v Script to create all the needed images from a master one.
  raw ==> 360x480, 300x400, 320x420
  Use ImageMagick
