import ReleaseTransformations._

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

name := """mongateau"""

lazy val root = (project in file("."))
  .enablePlugins(NewRelic, PlayScala, JavaServerAppPackaging, JDebPackaging, DebianDeployPlugin, RpmPlugin, RpmDeployPlugin)

scalaVersion := "2.11.6"

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

routesGenerator := InjectedRoutesGenerator

resolvers += Resolver.jcenterRepo

libraryDependencies ++= Seq(
  cache,
  ws,
  specs2 % Test
)

libraryDependencies ++= Seq(
  "org.apache.lucene" % "lucene-core" % "5.0.0",
  "org.apache.lucene" % "lucene-queryparser" % "5.0.0",
  "org.apache.lucene" % "lucene-analyzers-common" % "5.0.0",
  "org.webjars" %% "webjars-play" % "2.4.0-1",
  "org.webjars" % "bootstrap" % "3.3.6",
  "org.webjars" % "jquery" % "1.11.3"
)

newrelicLicenseKey := Some("85398abdccf3c591781511655fb366fd6dd28366")

maintainer in Linux := "Ugo Matrangolo <umatrangolo@gilt.com>"
packageSummary in Linux := "Mongateau"
packageDescription := "Listing page for Mongateau"
rpmRelease := "1"
rpmVendor := "mongateau.com"
rpmUrl := Some("https://github.com/umatrangolo/web-listing")
rpmLicense := Some("Apache v2")
rpmGroup := Some("Mongateau")
rpmBrpJavaRepackJars := true

maintainer := "Ugo Matrangolo <ugo.matrangolo@gmail.com>"
packageSummary := "Mongateau"
packageDescription := """Testing deb packaging for Mongateau"""
debianChangelog in Debian := Some(baseDirectory.value / "target" / s"mongateau_${version.value}_all.changes")

javaOptions in Universal ++= Seq(
  // -J params will be added as jvm parameters
  "-J-Xmx256m",
  "-J-Xms256m",
  "-J-XX:+PrintGCDetails",
  "-J-XX:+PrintGCDateStamps",
  "-J-XX:+PrintGCCause",
  "-J-XX:+PrintPromotionFailure",
  "-J-XX:+PrintCommandLineFlags",
  "-J-XX:+PrintTenuringDistribution",
  "-J-XX:+PrintClassHistogramBeforeFullGC",
  "-J-verbose:gc",
  s"-J-Xloggc:/var/log/${name.value}/gc.log",
  "-J-XX:+UseGCLogFileRotation",
  "-J-XX:GCLogFileSize=20m",
  "-J-XX:NumberOfGCLogFiles=10",
  "-J-XX:+DisableExplicitGC",
  "-J-XX:+UseConcMarkSweepGC",
  "-J-XX:+UseParNewGC",
  "-J-XX:+HeapDumpOnOutOfMemoryError",
  "-J-XX:+CMSClassUnloadingEnabled",
  "-J-XX:+UseCMSInitiatingOccupancyOnly",
  "-J-XX:CMSInitiatingOccupancyFraction=70",

  // others will be added as app parameters
  s"-Dpidfile.path=/var/run/${name.value}/play.pid",
  s"-Dconfig.file=/etc/${name.value}/application.production.conf",
  s"-Dlogger.file=/etc/${name.value}/logback.production.xml"
)

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  releaseStepTask(publish in Rpm),
  releaseStepTask(publish in Debian),
  setNextVersion,
  commitNextVersion,
  pushChanges
)
