import catalogs._
import controllers._
import models._
import play.api.GlobalSettings
import play.api._
import play.api.mvc._
import scala.concurrent.Future
import utils.AccessLoggingFilter

object Global extends WithFilters(AccessLoggingFilter)
