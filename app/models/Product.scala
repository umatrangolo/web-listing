package models

case class Image(name: String, format: String) {
  val Desktop = s"${name}_360x480.$format"
  val Mobile = s"${name}_300x400.$format"
  val Tablet = s"${name}_320x420.$format"
}

object Image {
  import play.api.libs.json._

  implicit val imageReads: Reads[Image] = Json.reads[Image]
  implicit val imageWrites: Writes[Image] = Json.writes[Image]
}

case class Product(
  key: String,
  name: String,
  description: String,
  image: Image,
  tags: Set[String] = Set.empty[String]
) {
  require(key.trim.size > 0)
}

object Product {
  import org.apache.lucene.document.Document
  import play.api.libs.json._

  implicit val productReads: Reads[Product] = Json.reads[Product]
  implicit val productWrites: Writes[Product] = Json.writes[Product]

  def fromDocument(doc: Document) = Product(
    key = doc.get("key"),
    name = doc.get("name"),
    description = doc.get("description"),
    image = Image(
      name = doc.get("image_name"),
      format = doc.get("image_format")
    ),
    tags = doc.get("tags").split('|').toSet
  )
}
