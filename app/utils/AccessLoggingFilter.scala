package utils

import play.api._
import play.api.GlobalSettings
import scala.concurrent.Future
import play.api.mvc._

object AccessLoggingFilter extends Filter {
  val accessLogger = Logger("access")

  def apply(next: (RequestHeader) => Future[Result])(request: RequestHeader): Future[Result] = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val now = System.currentTimeMillis
    val result = next(request) // invoke down the chain

    result.foreach { r =>
      val elapsed = System.currentTimeMillis - now
      val msg = s"method=${request.method} uri=${request.uri} remote-address=${request.remoteAddress} status=${r.header.status} time=$elapsed"
      accessLogger.info(msg)
    }

    result
  }
}
