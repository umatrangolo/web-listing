package catalogs

import java.util.concurrent._
import javax.inject._
import models._
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.document.{ Field, TextField, StoredField, FloatField, Document }
import org.apache.lucene.index.{ IndexWriter, IndexWriterConfig, DirectoryReader }
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.{ IndexSearcher, TopDocs }
import org.apache.lucene.store.Directory
import play.api._
import play.api.libs.json._
import scala.io.Source

trait Catalog {
  def search(query: String): Iterable[Product]
  def navTags: Iterable[String]
}

private[catalogs] object Catalog {
  val ProductsFilename = "products.json"
  val TagsFilename = "tags.json"
}

private[catalogs] class CatalogLuceneImpl @Inject() (
  @Named("catalog") val catalogRootFilename: String,
  val directory: Directory,
  val analyzer: Analyzer
) extends Catalog {
  import Catalog._

  private[this] val logger = Logger(this.getClass)

  @volatile private[this] var indexSearcher = load()

  override def search(q: String): Iterable[Product] = {
    logger.debug(s"Searching for $q ...")

    val queryParser = new QueryParser("text", analyzer)
    val query = queryParser.parse(q)

    val hits: TopDocs = indexSearcher.search(query, null, 1000)
    if (hits.totalHits == 0) {
      logger.info(s"No hits for query: $q")
      Seq.empty[Product]
    } else {
      val matches = hits.scoreDocs.map { hit => indexSearcher.doc(hit.doc) }.map { Product.fromDocument(_) }
      logger.debug(s"""Found ${hits.totalHits} hits for query: $query. Matches are:\n  ${matches.mkString("\n  ")}""")
      matches
    }
  }

  override val navTags: Iterable[String] = {
    val tags = (Json.parse(Source.fromFile(new java.io.File(s"$catalogRootFilename/$TagsFilename")).getLines.mkString) \ "tags").as[Iterable[String]]
      .map { tag => java.lang.Character.toUpperCase(tag.head) + tag.substring(1, tag.size)
    }
    logger.info(s"Navigation tags: $tags")
    tags
  }

  private[catalogs] def load() = {
    val now = System.currentTimeMillis
    logger.info(s"Loading all Products from $catalogRootFilename/$ProductsFilename ...")

    implicit val productReads = Product.productReads
    val productsJson = Json.parse(Source.fromFile(new java.io.File(s"$catalogRootFilename/$ProductsFilename")).getLines.mkString) \ "products"
    productsJson.validate[List[Product]] match {
      case s: JsSuccess[List[Product]] => {
        val products: List[Product] = s.get
        val indexWriter = new IndexWriter(directory, new IndexWriterConfig(analyzer))
        logger.debug(s"Products are: $products")

        products.foreach { product =>
          indexWriter.addDocument(docFrom(product))
          logger.debug(s"Added $product")
        }
        indexWriter.close()
      }
      case e: JsError => sys.error(s"Failed to load products from $ProductsFilename")
    }

    logger.info(s"Done. Took ${TimeUnit.SECONDS.convert(System.currentTimeMillis - now, TimeUnit.MILLISECONDS)}")
    new IndexSearcher(DirectoryReader.open(directory))
  }

  private def docFrom(product: Product): Document = {
    val doc = new Document()
    val text: List[String] = product.tags.toList
    doc.add(new Field("text", text.mkString(" "), TextField.TYPE_NOT_STORED))

    doc.add(new StoredField("key", product.key))
    doc.add(new StoredField("name", product.name))
    doc.add(new StoredField("description", product.description))
    doc.add(new StoredField("image_name", product.image.name))
    doc.add(new StoredField("image_format", product.image.format))
    doc.add(new StoredField("tags", product.tags.mkString("|")))

    doc
  }
}
