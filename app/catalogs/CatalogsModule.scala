package catalogs

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.store.{ Directory, RAMDirectory }
import play.api.{ Configuration, Environment }

class CatalogsModule(environment: Environment, configuration: Configuration) extends AbstractModule {
  override def configure() = {
    bind(classOf[String]).annotatedWith(Names.named("catalog")).toInstance(
      configuration.getString("catalog.directory").getOrElse(sys.error("Catalog directory has not been set"))
    )
    bind(classOf[Directory]).to(classOf[RAMDirectory])
    bind(classOf[Analyzer]).to(classOf[StandardAnalyzer])

    bind(classOf[Catalog]).to(classOf[CatalogLuceneImpl]).asEagerSingleton()
  }
}
