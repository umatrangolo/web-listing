package controllers

import catalogs._
import javax.inject._
import models.Product._
import models._
import play.api._
import play.api.libs.json._
import play.api.mvc._
import java.net.URL

class Products @Inject() (val catalog: Catalog, configuration: Configuration) extends Controller {
  private[this] val logger = Logger(this.getClass)

  private[this] val orderUrl = configuration.getString("urls.order").map(new URL(_)).getOrElse(sys.error("Unable to read order url from config"))
  private[this] val homeUrl = configuration.getString("urls.home").map(new URL(_)).getOrElse(sys.error("Unable to read home url from config"))

  def christening = listing("christening")
  def birthday = listing("birthday")
  def wedding = listing("wedding")
  def novelty = listing("novelty")
  def all = listing("*:*")

  def listing(query: String) = Action { implicit request =>
    Ok(views.html.listing(Redacted.get(query).getOrElse(query), catalog.search(query), catalog.navTags, orderUrl, homeUrl))
  }

  private val Redacted = Map(
    "*:*" -> "All"
  )
}
